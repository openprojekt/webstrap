const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin');
const SpritePlugin = require('svg-sprite-loader/plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default
const imageminMozjpeg = require('imagemin-mozjpeg');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const webpack = require('webpack');
const path = require('path');
const WorkboxPlugin = require('workbox-webpack-plugin');
const WebpackPwaManifest = require('webpack-pwa-manifest');

// Add more js files here to build
let jsAssets = [
    path.join(__dirname, 'src/js') + '/app.js',
]

module.exports = () => {
    return {
        mode: 'development',
        // watch: true,

        entry:{ bootstrap:['bootstrap-loader'],app: jsAssets},

        output: {
            path: path.join(__dirname, 'dist'),
            filename: '[name].js',
        },
        optimization: {
            // nodeEnv: 'production',
            // minimize: true,
            // flagIncludedChunks: true,
            minimizer: [
                new UglifyJsPlugin({
                    minify(file, sourceMap) {
                        // https://github.com/mishoo/UglifyJS2#minify-options
                        const uglifyJsOptions = {
                            output: {
                                comments: false
                            }
                        };

                        if (sourceMap) {
                            uglifyJsOptions.sourceMap = {
                                content: sourceMap,
                            };
                        }

                        return require('uglify-js').minify(file, uglifyJsOptions);
                    }
                })
            ]
        },
        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            // publicPath: path.join(__dirname, 'dist'),
            compress: true,
            inline: true,
            watchContentBase: true,
            port: 9002,
            index: 'index.html'
        },
        plugins: [
            new CleanWebpackPlugin(['dist']),
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                'window.jQuery': 'jquery',
                Tether: 'tether',
                'window.Tether': 'tether',
                Alert: 'exports-loader?Alert!bootstrap/js/dist/alert',
                Button: 'exports-loader?Button!bootstrap/js/dist/button',
                Carousel: 'exports-loader?Carousel!bootstrap/js/dist/carousel',
                Collapse: 'exports-loader?Collapse!bootstrap/js/dist/collapse',
                Dropdown: 'exports-loader?Dropdown!bootstrap/js/dist/dropdown',
                Modal: 'exports-loader?Modal!bootstrap/js/dist/modal',
                Popover: 'exports-loader?Popover!bootstrap/js/dist/popover',
                Scrollspy: 'exports-loader?Scrollspy!bootstrap/js/dist/scrollspy',
                Tab: 'exports-loader?Tab!bootstrap/js/dist/tab',
                Tooltip: 'exports-loader?Tooltip!bootstrap/js/dist/tooltip',
                Util: 'exports-loader?Util!bootstrap/js/dist/util',
            }),
            new ExtractTextPlugin({ filename: 'css/app.css', allChunks: true }),
            //https://github.com/kisenka/svg-sprite-loader
            new SpritePlugin(),
            //https://github.com/NMFR/optimize-css-assets-webpack-plugin/blob/master/README.md
            new OptimizeCssAssetsPlugin({
                assetNameRegExp: /.\.css$/g,
                cssProcessor: require('cssnano'),
                cssProcessorPluginOptions: {
                    preset: ['default', { discardComments: { removeAll: true } }],
                },
                canPrint: true
            }),
            //https://www.npmjs.com/package/imagemin-webpack-plugin
            new ImageminPlugin({
                test: /\.(jpe?g|png|gif)$/i,
                cacheFolder: path.join(__dirname, 'cache'),
                plugins: [
                    imageminMozjpeg({
                        quality: 100,
                        progressive: true
                    })
                ],

            }),
            // https://github.com/jantimon/html-webpack-plugin/blob/master/README.md
            new HtmlWebpackPlugin({
                template: 'src/index.html',
                minify: true
            }),
            new WorkboxPlugin.GenerateSW({
                // these options encourage the ServiceWorkers to get in there fast
                // and not allow any straggling "old" SWs to hang around
                clientsClaim: true,
                skipWaiting: true
            }),
            new WebpackPwaManifest({
                name: 'My Applications Friendly Name',
                short_name: 'Application',
                description: 'Description!',
                background_color: '#01579b',
                theme_color: '#01579b',
                'theme-color': '#01579b',
                start_url: '/',
                icons: [
                    {
                        src: path.resolve('src/img/icon-cc0.jpg'),
                        sizes: [96, 128, 192, 256, 384, 512],
                        destination: path.join('img', 'icons')
                    }
                ]
            })
        ],
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules|test|dist/,
                    loader: 'babel-loader',
                },
                {
                    test: /bootstrap[\/\\]dist[\/\\]js[\/\\]umd[\/\\]/,
                    loader: 'imports-loader?jQuery=jquery'
                },
                {
                    test: /\.(woff2?)$/,
                    loader: 'url-loader?limit=10000'
                },
                {
                    test: /\.(ttf|eot)$/,
                    loader: 'file-loader'
                },
                //https://github.com/kisenka/svg-sprite-loader
                {
                    test: /\.svg$/,
                    use: [
                        {
                            loader: 'svg-sprite-loader',
                        },
                        'svgo-loader'
                    ]
                },
                {
                    test: /\.(scss)$/,
                    use: [
                        {
                            loader: 'style-loader', // inject CSS to page
                        },
                        {
                            loader: 'css-loader', // translates CSS into CommonJS modules
                        },
                        {
                            loader: 'postcss-loader', // Run post css actions
                            options: {
                                plugins: function () { // post css plugins, can be exported to postcss.config.js
                                    return [
                                        require('precss'),
                                        require('autoprefixer')
                                    ]
                                }
                            }
                        },
                        {
                            loader: 'sass-loader' // compiles Sass to CSS
                        }
                    ]
                },
               {
                   test: /\.(png|jpg|gif)$/,
                   use: [
                       {
                           loader: 'file-loader',
                           options: {
                               name: 'img/[name].[ext]',
                               publicPath: '/'
                           }
                       }
                   ]
               },
                {
                    test: /\.html$/,
                    use: [
                        {
                            loader: 'html-loader',
                            options: {
                                minimize: true,
                                removeComments: false,
                                collapseWhitespace: false
                            }
                        },
         //               'file-loader?name=../[name].[ext]&!extract-loader!html-loader'
                    ]
                },
            ]
        },
      //  devtool: 'source-map'
    }
}
